package com.todo.controller.rest;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todo.dao.TodoRepository;
import com.todo.exception.ResourceNotFoundException;
import com.todo.model.Todo;

@RestController
@RequestMapping("api")
@CrossOrigin("*")
public class TodoController {

	@Autowired
	private TodoRepository todoRepository;

	@GetMapping("/todos")
	public List<Todo> getAllTodos() {
		return todoRepository.findAll();
	}

	@GetMapping("/todos/{id}")
	public ResponseEntity<Todo> getTodoById(@PathVariable(value = "id") Integer Id)
			throws ResourceNotFoundException {
		Todo todo = todoRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Todo not found for this id :: " + Id));
		return ResponseEntity.ok().body(todo);
	}

	@PostMapping("/todos")
	Todo createOrSaveTodo(@RequestBody Todo newTodo) {
		return todoRepository.save(newTodo);
	}

	@PutMapping("/todos/{id}")
	public ResponseEntity<Todo> updateTodo(@PathVariable(value = "id") Integer Id,
			@Valid @RequestBody Todo todoDetails) throws ResourceNotFoundException {
		Todo todo = todoRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Todo not found for this id :: " + Id));

		todo.setTitle(todoDetails.getTitle());
		todo.setCompleted(todoDetails.getCompleted());

		Todo updatedTodo = todoRepository.save(todo);
		return ResponseEntity.ok(updatedTodo);
	}
	
	@DeleteMapping("/todos/{id}")
	public void delete(@PathVariable(value = "id") Integer id) {
		todoRepository.deleteById(id);
	}
	
}
